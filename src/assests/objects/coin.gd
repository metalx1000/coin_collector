extends Control

@onready var hit = preload("res://assests/objects/hit.tscn")
@onready var points = preload("res://assests/objects/points/points.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func _on_area_2d_mouse_entered():
	get_tree().get_nodes_in_group("game")[0].new_score += 100
	var death = hit.instantiate()
	get_tree().get_root().add_child(death)
	death.position = position
	
	var point = points.instantiate()
	get_tree().get_root().add_child(point)
	point.position = position
	queue_free()
