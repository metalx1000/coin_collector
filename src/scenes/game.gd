extends Node2D
@onready var coins = preload("res://assests/objects/coin.tscn")
@onready var timer = $Timer
@onready var HUD = $HUD
@onready var HUD_TIMER = $hud_timer
@onready var coin_snd = $coin_snd

var score = 0
var new_score = 0
# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func hud_update():
	if score < new_score:
		coin_snd.play()
		score += 10
		
	HUD.text = "Score " + str(score)

func create_coin():
	timer.wait_time = randf_range(.2,1.5)
	var width = get_viewport_rect().size.x
	var height = get_viewport_rect().size.y
	var offset = 64
	var x = randi_range(10, width-offset)
	var y = randi_range(10, height-offset)
	var coin = coins.instantiate()
	coin.position = Vector2(x,y)
	add_child(coin)


func _on_exit_pressed():
	get_tree().change_scene_to_file("res://scenes/menu.tscn")
